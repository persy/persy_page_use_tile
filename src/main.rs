#[cfg(feature = "main")]
fn main() -> Result<(), Box<dyn std::error::Error>> {
    use clap::Parser;

    #[derive(Parser)]
    #[command(version, about)]
    struct Cli {
        input: String,
        output: String,
        #[arg(default_value_t = 1024)]
        block_bytes: u64,
        #[arg(default_value_t = 64)]
        line_blocks: usize,
    }
    let args = Cli::parse();
    persy_page_use_tiles::disk_usage_tile_settings(
        std::fs::OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(args.output)?,
        args.input,
        args.block_bytes,
        args.line_blocks,
    )?;
    Ok(())
}

#[cfg(not(feature = "main"))]
fn main() {}
