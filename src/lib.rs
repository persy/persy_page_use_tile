use persy::{
    inspect::{PageStateIter, PersyInspect},
    OpenOptions,
};
use std::{error::Error, io::Write, path::Path, result::Result, vec::IntoIter};
use tileline::{tile, Config, Element};

struct PageIterator {
    current: PageStateIter,
    block_size: u64,
    line_blocks: usize,
    elements: Vec<BlockPage>,
    current_size: u64,
    free_bytes: u64,
    use_bytes: u64,
}
impl PageIterator {
    fn new(current: PageStateIter, block_size: u64, line_blocks: usize) -> Self {
        Self {
            current,
            block_size,
            line_blocks,
            elements: Vec::new(),
            current_size: 0,
            use_bytes: 0,
            free_bytes: 0,
        }
    }
}

impl Iterator for PageIterator {
    type Item = PageLine;

    fn next(&mut self) -> Option<Self::Item> {
        while self.elements.len() < self.line_blocks {
            if let Some(s) = self.current.next() {
                self.current_size += s.size();
                if s.is_free() {
                    self.free_bytes += s.size();
                } else {
                    self.use_bytes += s.size();
                }
                if self.current_size > self.block_size {
                    for _x in 0..(self.current_size / self.block_size) {
                        self.elements.push(BlockPage::new(
                            self.free_bytes,
                            self.use_bytes,
                            self.block_size,
                        ));
                        if s.is_free() {
                            self.free_bytes = self.current_size;
                            self.use_bytes = 0;
                        } else {
                            self.free_bytes = 0;
                            self.use_bytes = self.current_size;
                        }
                    }
                    self.current_size = self.current_size % self.block_size;
                    if self.current_size != 0 {
                        if s.is_free() {
                            self.free_bytes += self.current_size;
                        } else {
                            self.use_bytes += self.current_size;
                        }
                    }
                }
            } else {
                break;
            }
        }
        if self.elements.is_empty() {
            None
        } else if self.elements.len() > self.line_blocks as usize {
            let iter = self
                .elements
                .drain(0..(self.line_blocks as usize))
                .collect::<Vec<_>>()
                .into_iter();
            Some(PageLine { elements: iter })
        } else {
            let iter = std::mem::take(&mut self.elements).into_iter();
            Some(PageLine { elements: iter })
        }
    }
}

struct PageLine {
    elements: IntoIter<BlockPage>,
}

impl Iterator for PageLine {
    type Item = BlockPage;

    fn next(&mut self) -> Option<Self::Item> {
        self.elements.next()
    }
}
struct BlockPage {
    color: tileline::Rgb,
}
impl BlockPage {
    fn new(free_count: u64, use_count: u64, block: u64) -> Self {
        println!("free: {} use : {}", free_count, use_count);
        let free_color = free_count as f64 / block as f64 * 255.0;
        let use_color = use_count as f64 / block as f64 * 255.0;
        let color = tileline::Rgb::new(0.0, free_color, use_color, None);
        Self { color }
    }
}
impl Element for BlockPage {
    fn get_color(&self) -> tileline::Rgb {
        self.color.clone()
    }

    fn get_border_color(&self) -> tileline::Rgb {
        tileline::Rgb::new(100.0, 100.0, 100.0, None)
    }

    fn get_link(&self) -> Option<Box<dyn tileline::ElementLink>> {
        None
    }
}

pub fn disk_usage_tile_settings<W, P>(
    output: W,
    file: P,
    block: u64,
    line: usize,
) -> Result<(), Box<dyn Error>>
where
    W: Write,
    P: AsRef<Path>,
{
    let persy = OpenOptions::new().open(file)?;

    let iter = persy.page_state_scan()?;

    tile(
        Config::new().build(),
        PageIterator::new(iter, block, line),
        output,
    )?;

    Ok(())
}
pub fn disk_usage_tile<W, P>(output: W, file: P) -> Result<(), Box<dyn Error>>
where
    W: Write,
    P: AsRef<Path>,
{
    disk_usage_tile_settings(output, file, 1024, 64)
}
