
Simple lib and main to generate a SVG that represent the free and used space of a Persy file.

### Install

```sh
cargo install --path="./" --features="main"
```

### Usage

```sh
persy_page_use_tiles ./file.persy output.svg
```
